import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutUsComponent } from './features/components/about-us/about-us.component';
import { ImagesComponent } from './features/container/images/images.component';
import { EventsComponent } from './features/container/events/events.component';
import { DragonBallsComponent } from './features/container/dragon-balls/dragon-balls.component';
import { RoomComponent } from './features/components/room/room.component';
import { SlideComponent } from './features/outline/slide/slide.component';
import { KwartoComponent } from './features/outline/kwarto/kwarto.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    ImagesComponent,
    EventsComponent,
    AboutUsComponent,
    DragonBallsComponent,
    RoomComponent,
    SlideComponent,
    DragonBallsComponent,
    RoomComponent,
    KwartoComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
